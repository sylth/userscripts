// ==UserScript==
// @name         [IdleScape] Stats
// @namespace    https://gitlab.com/sylth/userscripts/
// @version      0.41
// @description  Add a stats panel on IdleScape.com (currently display gold/time)
// @author       Sytha
// @match        https://idlescape.com/game
// @grant        none
// @updateURL    https://gitlab.com/sylth/userscripts/-/raw/master/idlescape.com-stats.js
// @downloadURL  https://gitlab.com/sylth/userscripts/-/raw/master/idlescape.com-stats.js
// @icon         https://www.google.com/s2/favicons?domain=idlescape.com
// ==/UserScript==

(function() {
    'use strict';

    function goldPerHour() {
        const goldStatsContainer = document.getElementById('goldStatsContainer'),
              goldStatsToggle = document.getElementById('goldStatsToggle'),
              goldStatsTable = document.getElementById('goldStatsTable');
        if (null !== goldStatsContainer && (null === goldStatsToggle || false === goldStatsToggle.checked)) {
            if (null !== goldStatsTable) goldStatsTable.style.display = 'none';
            return;
        }

        document.querySelectorAll('.item-log-category-closed').forEach(element => element.click());

        if (null !== goldStatsTable) goldStatsTable.style.display = 'table';
        const roundPrecision = 0;
        const elapsed = document.querySelector('.item-log-timer').textContent;
        let hours = elapsed.match(/(\d+)h/); hours = (null !== hours ? parseInt(hours[1]) : 0);
        let minutes = elapsed.match(/(\d+)m/); minutes = (null !== minutes ? parseInt(minutes[1]) : 0);
        let seconds = elapsed.match(/(\d+)s/); seconds = (null !== seconds ? parseInt(seconds[1]) : 0);
        const totalSeconds = seconds + (minutes * 60) + (hours * 60 * 60);
        let totalGold = 0;
        document.querySelectorAll('.item-log-item').forEach(logItem => {
            let gold = logItem.textContent.match(/Gold x (\d+)/);
            if (null !== gold) totalGold += parseInt(gold[1]);
        });

        const round = (value, precision) => parseInt(value * Math.pow(10, precision)) / Math.pow(10, precision);
        const nbFormat = nb => {
            const nbParts = String(nb).split('.');
            let nbParts0Fmt = '';
            String(nbParts[0]).split('').reverse().forEach( (value, index) => {
                if (0 === (index+1) % 3) value = ' ' + value;
                nbParts0Fmt = value + nbParts0Fmt;
            });
            nbParts[0] = nbParts0Fmt.trim();
            if (2 === nbParts.length) return nbParts[0] + ',' + nbParts[1];
            return nbParts[0];
        };

        const goldPerSeconds = totalGold / totalSeconds,
              goldPerMinutes = goldPerSeconds * 60,
              goldPerHours = goldPerMinutes * 60,
              goldPerDays = goldPerHours * 24;
        const formattedValues = {
            goldPerSeconds: nbFormat(round(goldPerSeconds, roundPrecision)),
            goldPerMinutes: nbFormat(round(goldPerMinutes, roundPrecision)),
            goldPerHours: nbFormat(round(goldPerHours, roundPrecision)),
            goldPerDays: nbFormat(round(goldPerDays, roundPrecision))
        };

        const rowTemplate = '<tr><th style="padding: 0 0 0 2px">%LABEL%</th><td style="padding:0 8px 0;text-align:right">%VALUE%</td></tr>';
        const goldStatsToggleChecked = null !== goldStatsToggle ? goldStatsToggle.checked : true;

        let goldStats_html = `<div id="goldStatsContainer">
            <center style="font-weight: bold; margin: 5px 0 10px 0; padding-bottom: 10px; border-bottom: 1px solid;">
            <input id="goldStatsToggle" type=checkbox> <label style="margin: 0" for="goldStatsToggle">Gold in ${elapsed}</label>
            </center>
            <table id="goldStatsTable" style=margin-bottom:10px><tbody>
                ${rowTemplate.replace('%LABEL%', 'Seconds').replace('%VALUE%', formattedValues.goldPerSeconds)}
                ${rowTemplate.replace('%LABEL%', 'Minutes').replace('%VALUE%', formattedValues.goldPerMinutes)}
                ${rowTemplate.replace('%LABEL%', 'Hours').replace('%VALUE%', formattedValues.goldPerHours)}
                ${rowTemplate.replace('%LABEL%', 'Days').replace('%VALUE%', formattedValues.goldPerDays)}
            <tbody></table>
        </div>`;
        if (null === goldStatsContainer) {
            document.querySelector('.nav-drawer-logo').insertAdjacentHTML('beforeBegin', goldStats_html);
            document.querySelector('.nav-drawer-logo').remove();
        } else {
            goldStatsContainer.outerHTML = goldStats_html;
        }

        document.getElementById('goldStatsToggle').checked = goldStatsToggleChecked;
    }
    setInterval(goldPerHour, 1000);
})();

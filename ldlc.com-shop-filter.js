// ==UserScript==
// @name         [LDLC] Boutiques Switcher
// @namespace    https://gitlab.com/sylth/userscripts/
// @version      0.2
// @description  Add a new custom filter "boutiques" on the listing pages
// @author       Sytha
// @match        https://www.ldlc.com/*
// @icon         https://www.google.com/s2/favicons?domain=ldlc.com
// @grant        none
// @updateURL    https://gitlab.com/sylth/userscripts/-/raw/master/ldlc.com-shop-filter.js
// @downloadURL  https://gitlab.com/sylth/userscripts/-/raw/master/ldlc.com-shop-filter.js

// ==/UserScript==

(function() {
    'use strict';

    if (null === document.getElementById('filterProduct')) return;

    /* Update this list with the shops you want */
    const boutiques = {
        '470050': 'Bordeaux Mérignac',
        '470088': 'Bordeaux Pasteur',
        '470041': 'Limoges',
        '470040': 'Angoulême',
    };

    const currentUrlSplit = window.location.href.split('+');

    let boutiquesOptions = '';
    for (let boutiqueId in boutiques) {
        boutiquesOptions += `<option id="customer-filter-option-boutique-${boutiqueId}" value="${boutiqueId}">${boutiques[boutiqueId]}</option>`;
    }

    function insertFilter() {
        if (null !== document.getElementById('customer-filter-boutiques-container')) return;

        const currentUrlSplit = window.location.href.split('+');
        const container = document.querySelector('#filterProduct .filters');
        container.insertAdjacentHTML('afterBegin', `<div id="customer-filter-boutiques-container" class="filter pointer">
    <h2>Disponible en boutique <button id="custom-filter-fetch-quantities" style="font-size: 0.8em; padding: 0 !important; line-height: 1em;">fetch quantities</button></h2>
    <select style="width: 100%; background: #F8F8F8; color: #505050; border: 1px solid #E1E1E1; padding: 5px;" id="custom-filter-boutiques">
    <option value="">non (stocks site)</option>
    ${boutiquesOptions}
    </div>`);
        document.getElementById('custom-filter-boutiques').addEventListener('change', event => {
            event.stopPropagation(); event.preventDefault();

            if ('' === event.target.value) {
                window.location.href = currentUrlSplit[0];
                return;
            }

            fetch(`https://www.ldlc.com/v4/fr-fr/stock/set-favorite-shop/${event.target.value}/shopsmap`, {method: 'POST', 'headers': {'x-requested-with': 'XMLHttpRequest'} })
                .then(result => result.json())
                .then(json => {
                if ('status' in json && 'OK' === json.status) {
                    window.location.href = currentUrlSplit[0] + `+fsa${event.target.value}-1.html`;
                }
            });
        });
        document.getElementById('custom-filter-fetch-quantities').addEventListener('click', event => {
            event.target.remove();
            const boutiquesPromises = [];
            for (let boutiqueId in boutiques) {
                boutiquesPromises.push(fetch(currentUrlSplit[0] + `+fsa${boutiqueId}-1.html`).then(response => response.text()));
            }
            Promise.all(boutiquesPromises).then( htmls => {
                for (let html of htmls) {
                    const parser = (new DOMParser).parseFromString(html, 'text/html');
                    const listTitle = parser.querySelector('.head-list .title-2');
                    const boutiqueId = parser.querySelector('#category[data-filter]').getAttribute('data-filter').match(/fsa(\d+)/)[1];
                    const boutiqueOption = document.getElementById('customer-filter-option-boutique-' + boutiqueId);
                    let titleCount = listTitle.textContent.split(' ')[0].toLocaleLowerCase();
                    if ('un' === titleCount) titleCount = '1';
                    else if ('aucune' === titleCount) titleCount = '0';
                    const boutiqueCount = ' (' + titleCount + ')';
                    boutiques[boutiqueId] += boutiqueCount;
                    if (null !== boutiqueOption) {
                        boutiqueOption.textContent += boutiqueCount;
                    }
                }
            });
        });
    }
    insertFilter();
    setInterval(insertFilter, 200);

    for (let split of currentUrlSplit) {
        const currentBoutiqueId = split.match(/fsa(\d+)\-/);
        if (null !== currentBoutiqueId && undefined !== currentBoutiqueId[1]) {
            if (null !== document.getElementById('custom-filter-boutiques')) {
                document.getElementById('custom-filter-boutiques').value = currentBoutiqueId[1];
            }
            const lastBreadCrumb = document.querySelector('h1.lastBreadcrumb');
            if (null !== lastBreadCrumb) {
                lastBreadCrumb.innerHTML = lastBreadCrumb.innerHTML.replace(/boutique.*/, 'boutique (' + boutiques[currentBoutiqueId[1]] + ')');
            }
        }
    }
})();

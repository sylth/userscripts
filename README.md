# HOW TO USE USERSCRIPTS?

Install a userscript extension, eg. [TamperMonkey](https://www.tampermonkey.net/)
- ![firefox](https://www.google.com/s2/favicons?domain=firefox.com) https://addons.mozilla.org/fr/firefox/addon/tampermonkey/
- ![chrome](https://www.google.com/s2/favicons?domain=chrome.com) https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo

// ==UserScript==
// @name         [Amazon.fr] Price change viewer
// @namespace    https://gitlab.com/sylth/userscripts/
// @version      0.2
// @description  Format the price diff in the cart
// @author       Sytha
// @match        https://www.amazon.fr/gp/cart/*
// @grant        none
// @updateURL    https://gitlab.com/sylth/userscripts/-/raw/master/amazon.fr-price-change-viewer.js
// @downloadURL  https://gitlab.com/sylth/userscripts/-/raw/master/amazon.fr-price-change-viewer.js
// ==/UserScript==

(function() {
    function computeCartChanges() {
        // Checks
        const cartChangesContainer = document.getElementById('cart-important-message-box');
        if (null === cartChangesContainer) return;
        const elements = cartChangesContainer.querySelectorAll('ul li');
        if (0 === elements.length) return;

        // Helpers
        const	parsePriceText = priceText => parseFloat(priceText.replace(',', '.').replace('€', '').trim()),
              round = (price, pre) => parseInt(price * Math.pow(10, pre)) / Math.pow(10, pre),
              nbFormat = nb => {
                  const nbParts = String(nb).split('.');
                  let nbParts0Fmt = '';
                  String(nbParts[0]).split('').reverse().forEach( (value, index) => {
                      if (0 === (index+1) % 3) value = ' ' + value;
                      nbParts0Fmt = value + nbParts0Fmt;
                  });
                  nbParts[0] = nbParts0Fmt.trim();
                  if (2 === nbParts.length) return nbParts[0] + ',' + nbParts[1];
                  return nbParts[0];
              },
              formatPrice = (price, pre) => {
                  const priceRound = round(price, pre),
                        priceText = String(priceRound).split('.');
                  if (undefined === priceText[1]) priceText[1] = '0'.repeat(pre);
                  else if (pre !== priceText[1].length) priceText[1] += '0'.repeat(pre - priceText[1].length);
                  priceText[0] = nbFormat(priceText[0])
                  return priceText.join(',') + ' €';
              };
        ;

        // Inject custom style
        cartChangesContainer.insertAdjacentHTML('afterBegin', `<style id="computed-price-diff-style">
        .computed-price-diff { display: inline-block; font-family: monospace; text-align: right; font-size: 0.9em; }
        .computed-price-diff::before { content: " [ "; float: left; }
        .computed-price-diff:after { content: "]"; }
        </style>`);

        // Foreach "price has changed" element
        elements.forEach((element, index) => {
            const prices = element.querySelectorAll('.sc-price');
            if (prices.length < 2) return;
            const priceBefore = parsePriceText(prices[0].textContent),
                  priceAfter = parsePriceText(prices[1].textContent),
                  priceDiff = priceBefore - priceAfter,
                  priceDiffText = formatPrice(-1 * priceDiff, 2),
                  diffColor = priceDiff > 0 ? 'rgb(60, 160, 0)' : 'rgb(220, 60, 0)',
                  bgColor = priceDiff > 0 ? 'rgba(60, 160, 0, 0.1)' : 'rgba(220, 60, 0, 0.1)',
                  sign = priceDiff < 0 ? '+' : '';
            element.insertAdjacentHTML('afterBegin', `<span class="computed-price-diff" style="background-color: ${bgColor}; color: ${diffColor};">${sign}${priceDiffText}</span>`);
        });

        // Format the price diff containers
        const priceDiffs = document.querySelectorAll('.computed-price-diff');
        if (0 === priceDiffs.length) return;
        let maxWidth = 0;
        priceDiffs.forEach(element => {
            const width = element.getClientRects()[0].width;
            if (width > maxWidth) maxWidth = width;
        });
        priceDiffs.forEach(element => {
            element.style.width = maxWidth + 'px';
        });
    }

    // Compute after 2 seconds, because.
    setTimeout(computeCartChanges, 2000);
})();
